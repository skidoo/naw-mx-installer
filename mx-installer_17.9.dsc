-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: mx-installer
Binary: mx-installer
Architecture: any
Version: 17.9
Maintainer: anticapitalista <antiX@operamail.com>
Homepage: http://mxlinux.org
Standards-Version: 3.9.4
Build-Depends: qt5-qmake, qt5-default, qttools5-dev-tools, debhelper (>= 9.20120115)
Package-List:
 mx-installer deb system optional arch=any
Checksums-Sha1:
 83766a696a9ffb0eca3cfb9dbfbb828119eb71b2 352920 mx-installer_17.9.tar.xz
Checksums-Sha256:
 88d5fef4959937e00cfa303c56e2bc4a1b0d2bedb9da73be67a9d859db8539a4 352920 mx-installer_17.9.tar.xz
Files:
 7e6e8d7b8bd5d9ce0af87b1d896dd33a 352920 mx-installer_17.9.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJZzUavAAoJEHCTjHgGee6YGMwH/iMCOpWESzqW+3JV25A/hQP0
/Uww40hRA0UYXJG3p8WfQHc+Q5x1XxHTBVuDZDDLBiej8vjFqdVn6aZvwM7QAQUO
e9CUUy79H/Y0C2OWDtOXms80YBKzCch+XTiXh7jArvbamujeR8JVgVH/UGB/ocCf
8E04OrXZJYcSMCQIhSWrsEYwOmHTwOHldGMhjj/j9TDIi0M77QT9sPlsVj8RFf0n
EtyhAHT0PD7vM1Ae8Ta43GIwqcgFxOzQVtwvjorXM3o82xW9thd+2/7kN5QRg1to
JPRnqDXp2MCwoYxSwFSMlLKrXWejYRL6NXM/L+lV9ABxHFubdidTETyda0w3Nrg=
=oZ5u
-----END PGP SIGNATURE-----
